from turtle import Turtle
MOVE_SPEED = 10


class Player(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.shape("turtle")
        self.color("black")
        self.up()
        self.repos()


    def move_up(self):
        self.fd(MOVE_SPEED)


    def move_down(self):
        self.bk(MOVE_SPEED)


    def repos(self):
        self.goto(0, -380)
        self.seth(90)
        self.showturtle()