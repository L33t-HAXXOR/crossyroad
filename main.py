#IMPORTS
from turtle import Screen
from car import CarMng
from player import Player
import random as q
from level import Level
import time

#VARS & CONST
screen = Screen()
screen.setup(800, 800)
screen.title("Crossy")
screen.tracer(0)

froggy = Player()
mngr = CarMng()
lvl = Level()
sleeper = 0.1
game = True

screen.listen()
screen.onkey(key="w", fun=froggy.move_up)
screen.onkey(key="Up", fun=froggy.move_up)
screen.onkey(key="x", fun=screen.bye)

i = 0
while game:
    time.sleep(sleeper)
    mngr.make_mobs()
    mngr.move_em()
    for x in mngr.cars_arr:
        if froggy.ycor() >= -320:
            if froggy.distance(x) < 19:
                print("game over")
                lvl.u_ded()
                game = False

    # Level Change
    if froggy.ycor() >= 400:
        froggy.repos()
        mngr.lvl_up()
        lvl.score += 1
        lvl.score_update()
    screen.update()




screen.exitonclick()