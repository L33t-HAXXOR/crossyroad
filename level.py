from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Arial", 20, "normal")

class Level(Turtle):
    def __init__(self):
        super().__init__()
        self.up()
        self.hideturtle()
        self.goto(-350, 360)
        self.score = 0
        self.score_update()


    def score_update(self):
        self.clear()
        self.write(f"Level: {self.score}", font=FONT, align=ALIGNMENT)


    def u_ded(self):
        self.goto(0, -100)
        self.color("red")
        self.write("U ded", font=("Arial", 200, "normal"), align=ALIGNMENT)