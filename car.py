from turtle import Turtle, colormode
import random as rand
START_POS = [(0, 0), (20, 0)]
MOVE_DIST = 10

colormode(255)

class CarMng():
    def __init__(self):
        self.cars_arr = []
        self.speed = MOVE_DIST
        self.chance = rand.randint(1, 6)


    def make_mobs(self):
        chance = rand.randint(1, 6)
        if chance == 1:
            new_mob = Turtle("square")
            new_mob.shapesize(stretch_wid=1, stretch_len=2)
            new_mob.up()
            new_mob.seth(180)
            new_mob.color(self.color_gen())
            rand_y = rand.randrange(-320, 320, 20)
            new_mob.goto(400, rand_y)
            self.cars_arr.append(new_mob)


    def move_em(self):
        for mobby in self.cars_arr:
            if mobby.xcor() < -420:
                mobby.clear()
                self.cars_arr.remove(mobby)
            mobby.fd(self.speed)


    def lvl_up(self):
        self.speed += 10

    def color_gen(self):
        color = []
        while len(color) < 3:
            r = rand.randint(20, 200)
            g = rand.randint(20, 200)
            b = rand.randint(20, 200)
            if r not in color:
                color.append(r)
            if abs(r - g) > 20:
                color.append(g)
            else:
                self.color_gen()
            if r in color and g in color:
                if abs(r - b) > 20 and abs(g - b) > 20:
                    color.append(b)
                else:
                    self.color_gen()

        return (color[0], color[1], color[2])
